// IOTools provides some useful io implementations and tools.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package iotools

import (
	"io"
	"io/ioutil"
)

// eofreader implements an io.Reader with Read always giving an io.EOF.
type eofreader int

// EOFReader is an io.Reader with Read always giving an io.EOF.
const EOFReader = eofreader(0)

// exactreader provides an io.Reader that will return an error unless a specified number of bytes are read
type exactreader struct {
	r *io.LimitedReader
}

// ByteCountWriter wraps an io.Writer and keeps track of the number of bytes that have been written to the wrapped writer.
type ByteCountWriter struct {
	W io.Writer // The wrapped writer
	N int64     // The number of bytes written to the writer
}

/////////////////////////////////////////////////////////////////////////
// oefreader functions
/////////////////////////////////////////////////////////////////////////

// Read always returns io.EOF.
func (eofreader) Read(_ []byte) (int, error) {
	return 0, io.EOF
}

// WriteTo always returns 0 bytes of data written.
func (eofreader) WriteTo(w io.Writer) (int64, error) {
	return 0, nil
}

// ReadByte always returns io.EOF.
func (eofreader) ReadByte() (byte, error) {
	return 0, io.EOF
}

/////////////////////////////////////////////////////////////////////////
// exactreader functions
/////////////////////////////////////////////////////////////////////////

// ExactReader returns an io.Reader that reads from r but stops with io.EOF after n bytes, and returns an io.ErrUnexpectedEOF if io.EOF occurs before n bytes have been read.
func ExactReader(r io.Reader, n int64) io.Reader {
	if n == 0 {
		return EOFReader
	}
	return &exactreader{&io.LimitedReader{R: r, N: n}}
}

// Read implements io.Reader. The error err is set to io.ErrUnexpectedEOF if io.EOF occurs before the required number of bytes have been read.
func (r *exactreader) Read(p []byte) (n int, err error) {
	n, err = r.r.Read(p)
	if err == io.EOF && r.r.N != 0 {
		err = io.ErrUnexpectedEOF
	}
	return
}

// WriteTo writes data to w until there's no more data to write or when an error occurs. The return value n is the number of bytes written. Any error encountered during the write is also returned.
func (r *exactreader) WriteTo(w io.Writer) (n int64, err error) {
	n, err = io.Copy(w, r.r)
	if err == nil && r.r.N != 0 {
		err = io.ErrUnexpectedEOF
	}
	return
}

/////////////////////////////////////////////////////////////////////////
// ByteCountWriter functions
/////////////////////////////////////////////////////////////////////////

// Write writes len(p) bytes from p to the underlying data stream. It returns the number of bytes written from p (0 <= n <= len(p)) and any error encountered that caused the write to stop early.
func (w *ByteCountWriter) Write(p []byte) (int, error) {
	n, err := w.W.Write(p)
	w.N += int64(n)
	return n, err
}

// ReadFrom ReadFrom reads data from r until EOF or error. The return value n is the number of bytes read. Any error except EOF encountered during the read is also returned.
func (w *ByteCountWriter) ReadFrom(r io.Reader) (n int64, err error) {
	n, err = io.Copy(w.W, r)
	w.N += n
	return
}

// Size returns the number of bytes that have been written to the writer.
func (w *ByteCountWriter) Size() int64 {
	return w.N
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Drain reads from r until either io.EOF is reached on r or an error occurs. It returns the number of bytes read and the first error encountered while reading, if any. Note that on completing successful drain the second return value is nil and not io.EOF. Because Drain is defined to read from r until io.EOF, it does not treat an io.EOF from Read as an error to be reported.
func Drain(r io.Reader) (int64, error) {
	return io.Copy(ioutil.Discard, r)
}
